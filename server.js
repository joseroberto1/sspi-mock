const https = require("https");
const fs = require("fs");
const constants = require("constants");

const options = {
  key: fs.readFileSync("./certs/spi_key.pem"),
  cert: fs.readFileSync("./certs/spi_cert.pem"),
  ca: fs.readFileSync("./certs/ca_cert.pem"),
  secureOptions: constants.SSL_OP_NO_SSLv2 | constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_SSLv3 | constants.SSL_OP_NO_TLSv1_1 | constants.SSL_OP_NO_TLSv1_3,
  ciphers: "ECDHE-RSA-AES128-GCM-SHA256",
  requestCert: true,
  rejectUnauthorized: true,
  enableTrace: false
};

function trace(log, onscreen=true)
{
    if(onscreen)
    {
        console.log(log);
    }
    
}

function printHeaderInfo(req)
{
    trace("url: " + req.url);
    trace("http version: " + req.httpVersion);
    trace("headers: ");
    for(var info in req.headers)
    {
        trace(info + ": " + req.headers[info]);
    }

    let body = [];
    req.on("data", (chunk) => {
        trace(chunk.length);
    body.push(chunk);
    }).on("end", () => {
    body = Buffer.concat(body).toString();
    trace(body.length);
    trace(body);
    if(body === "exit")
    {
        process.exit();
    }
    });
    trace(body.length);
    trace(body);
    
}

function sendResponse(req, res, body=true)
{
    res.writeHead(200);
    if(body)
    {
        for(var info in req.headers)
        {
            res.write(info + ": " + req.headers[info]+ "\n");
        }
    }
    res.end();
}

var server = https.createServer(options, function (req, res) {
    
  
    if(req.client.authorized)
    {
        trace("Authorized");
    }
    else{
        trace("Unauthorized");
    }

    if(req.method === "POST")
    {
        trace("POST");
        sendResponse(req, res);
        printHeaderInfo(req);
    }
    else if(req.method === "PUT")
    {
        trace("PUT");
        sendResponse(req, res);
        printHeaderInfo(req);
    }
    else if(req.method === "GET")
    {
        trace("GET");
        sendResponse(req, res, false);
        printHeaderInfo(req);
    }
    else if(req.method === "DELETE")
    {
        trace("DELETE");
        sendResponse(req, res, false);
        printHeaderInfo(req);
    }
    else
    {
        trace("ELSE");
        sendResponse(req, res);
        printHeaderInfo(req);
    }
    
  }).listen(8000);

  server.on("error", function(e){
      console.log(e);
  });