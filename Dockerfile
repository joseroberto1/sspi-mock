FROM keymetrics/pm2:8-stretch

WORKDIR /var/opt/sspi_mock
ADD . .
EXPOSE 8000
CMD [ "pm2-docker", "pm2-api.config.yaml"]
