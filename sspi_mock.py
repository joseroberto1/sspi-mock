import logging
import ssl
import sys

if (sys.version_info > (3, 0)):
    # Python 3 code in this block
    from http.server import HTTPServer, BaseHTTPRequestHandler
    bc=BaseHTTPRequestHandler
else:
    # Python 2 code in this block
    import BaseHTTPServer
    bc=BaseHTTPServer.BaseHTTPRequestHandler

#https://blog.anvileight.com/posts/simple-python-http-server/#example-with-ssl-support

def count_content(headers):
    content_len = 0
    for k,v in headers.items():
        content_len += len("{0}:{1}\n".format(k,v).encode())
    return content_len

def sendback_request_headers(c):
    for k,v in c.headers.items():
        c.wfile.write("{0}:{1}\n".format(k,v).encode())

class SimpleHTTPRequestHandler(bc):

    def do_GET(self):

        logging.error(self.headers)
        self.send_response(200)
        self.send_header("PI-ResourceId","UEkBbgR78VqKK/uvuq9O0r9bqXyBH9Ur")
        self.send_header("PI-Pull-Next", "/api/v1/out/11111111/stream/f0d744d792d6")
        self.send_header("Content-Length", "0")
        self.end_headers()

    def do_POST(self):
        logging.error(self.headers)
        if self.headers['Content-Length'] is not None:
            content_length = int(self.headers['Content-Length'])
        else:
            content_length = 0
        body = self.rfile.read(content_length)
        print(body)
        if(body.decode("utf-8").lower() == "exit".lower()):
            sys.exit(0)
        self.send_response(201)
        self.send_header("PI-ResourceId","UEkBbgR78VqKK/uvuq9O0r9bqXyBH9Ur")
        response_content_len = count_content(self.headers)
        self.send_header("Content-Length", str(response_content_len))
        self.end_headers()
        sendback_request_headers(self)

    def do_PUT(self):
        self.do_POST()


    def do_DELETE(self):
        logging.error(self.headers)
        self.send_response(200)
        self.send_header("Content-Length", "0")
        self.end_headers()

import argparse

parser = argparse.ArgumentParser(description = 'Mockup server for SPI Get/Post/Delete API Tests.')

parser.add_argument('-t', '--http_version', help='http protocol version',
                    choices=['1.0', '1.1'], default='1.1')
if (sys.version_info > (3, 0)):
    parser.add_argument('-s', '--tls_version', help='tls protocol version',
                    choices=['1.1', '1.2'], default='1.2')
    parser.add_argument('-c', '--ciphers', help='ciphers enabled for tls',
                    default='ECDHE-RSA-AES128-GCM-SHA256')
parser.add_argument('-n', '--no_client_auth', help='disable client auth',
                    action='store_true')
parser.add_argument('-e', '--cert_file', help='certificate file (PEM)',
                    default='./certs/spi_cert.pem')
parser.add_argument('-k', '--key_file', help='key file (PEM)',
                    default='./certs/spi_key.pem')
parser.add_argument('-u', '--ca_file', help='ca certicate file (PEM)',
                    default='./certs/ca_cert.pem')
parser.add_argument('-p', '--port', help='listen port',
                    default=8000)
parser.add_argument('-d', '--server_address', help='server address to bind',
                    default='127.0.0.1')
parser.add_argument('-w', '--show_values', help='show loaded parameters',
                    action='store_true' )

args = parser.parse_args()

if (args.show_values):
    for k,v in vars(args).items():
        print ("{0:15}:\t{1:}".format(k,v))

s = SimpleHTTPRequestHandler
s.protocol_version="HTTP/{0}".format(args.http_version)

if (args.no_client_auth):
    cert_req = ssl.CERT_NONE
else:
    cert_req = ssl.CERT_REQUIRED

if (sys.version_info > (3, 0)):
    # Python 3 code in this block

    if (args.tls_version == '1.1'):
        tls_v = ssl.PROTOCOL_TLSv1_1
    else:
        tls_v = ssl.PROTOCOL_TLSv1_2

    httpd = HTTPServer((args.server_address, args.port), s)

    context = ssl.SSLContext(tls_v)
    context.check_hostname = False
    context.verify_mode = cert_req

    context.load_cert_chain(keyfile=args.key_file,
                            certfile=args.cert_file)
    context.load_verify_locations(cafile=args.ca_file)
    context.set_ciphers(args.ciphers)

    httpd.socket = context.wrap_socket (httpd.socket, server_side=True)

else:
    # Python 2 code in this block
    httpd = BaseHTTPServer.HTTPServer((args.server_address, args.port), s)

    httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile=args.key_file,
        certfile=args.cert_file,
        ca_certs=args.ca_file,
        cert_reqs=cert_req,
        server_side=True)

httpd.serve_forever()
