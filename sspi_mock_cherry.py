import ssl
import cherrypy

# ----------------------------------------------------------------------------

HTTP_STATUS_OK      = 200
HTTP_STATUS_CREATED = 201
HDR_RES_ID          = "PI-ResourceId"
HDR_P_NEXT          = "PI-Pull-Next"
HDR_RES_ID_DATA     = "UEkBbgR78VqKK/uvuq9O0r9bqXyBH9Ur"
HDR_P_NEXT_DATA     = "/api/v1/out/11111111/stream/f0d744d792d6"

# ----------------------------------------------------------------------------

@cherrypy.expose
class spi(object):
	def GET (self):
		cherrypy.response.status = HTTP_STATUS_OK
		cherrypy.response.headers[HDR_RES_ID] = HDR_RES_ID_DATA
		cherrypy.response.headers[HDR_P_NEXT] = HDR_P_NEXT_DATA
		return "<xml>\n\tdoc\n</xml>\n"

	def POST(self):
		cherrypy.response.status = HTTP_STATUS_CREATED
		cherrypy.response.headers[HDR_RES_ID] = HDR_RES_ID_DATA
		return ""

	def PUT(self):
		return self.POST()

	def DELETE(self):
		cherrypy.response.status = HTTP_STATUS_OK
		return ""

if __name__ == "__main__":
	spi_conf = {
		"global": {
			"server.socket_host": "0.0.0.0",
			"server.socket_port": 8000,
			"server.thread_pool": 32,
			"log.screen" : True,
			"server.ssl_module":"builtin",
			"server.ssl_certificate":"./certs/spi_cert.pem",
			"server.ssl_private_key":"./certs/spi_key.pem",
			"server.ssl_certificate_chain":"./certs/ca_cert.pem",
		}
	}

	conf = {
		"/": {
			"request.dispatch": cherrypy.dispatch.MethodDispatcher(),
			"tools.sessions.on": False,
			"tools.response_headers.on": True,
			"tools.sessions.secure": True,
			"tools.response_headers.headers": [("Content-Type", "*/*")]
		}
	}

	cherrypy.server.unsubscribe()
	svr = cherrypy._cpserver.Server()

	svr._socket_host          = spi_conf["global"]["server.socket_host"]
	svr.socket_port           = spi_conf["global"]["server.socket_port"]
	svr.ssl_module            = spi_conf["global"]["server.ssl_module"]
	svr.ssl_certificate       = spi_conf["global"]["server.ssl_certificate"]
	svr.ssl_private_key       = spi_conf["global"]["server.ssl_private_key"]
	svr.ssl_certificate_chain = spi_conf["global"]["server.ssl_certificate_chain"]

	svr.subscribe()

	cherrypy.tree.mount(spi(), "/", conf)
	cherrypy.config.update(spi_conf)

	cherrypy.engine.start()

	ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)

	ssl_context.check_hostname = False
	ssl_context.verify_mode    = ssl.CERT_REQUIRED

	ssl_context.load_cert_chain(svr.ssl_certificate, svr.ssl_private_key)
	ssl_context.load_verify_locations(svr.ssl_certificate_chain)
	ssl_context.set_ciphers("ECDHE-RSA-AES128-GCM-SHA256")

	svr.httpserver.ssl_adapter.context = ssl_context

	cherrypy.engine.block()

# ----------------------------------------------------------------------------
